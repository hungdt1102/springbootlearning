package com.example.restservice.controller;

import com.example.restservice.model.User;
import com.example.restservice.services.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Slf4j
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;


    @GetMapping("")
    public List<User> findAllUser(){
        log.info("Controller findAllUser");
        List<User> users = userService.getAllUserFromJsonFile();

        log.info("Controller findAllUser END: {}", users);
        return users;
    }

    @GetMapping("/{id}")
    public User findUserById(@PathVariable("id") Integer id){
        log.info("Find user by id: {}", id);
        return userService.getUserById(id);
    }

    @GetMapping("/data")
    public List<User> findAllUserFromAPI(){
        log.info("Controller findAllUser");
        List<User> users = userService.getAllUserFromRestAPI();

        log.info("Controller findAllUser END: {}", users);
        return users;
    }

    @GetMapping("/data/{id}")
    public User findUserByIdFromAPI(@PathVariable("id") Integer id){
        log.info("Find user by id: {}", id);
        return userService.getUserByIdFromRestAPI(id);
    }
}
