# Project learning Java
## How to run?
```
mvn clean spring-boot:run
```

**API Demo**
```
GetAllUser: http://localhost:6969/learning/users
GetUserById: http://localhost:6969/learning/users/1
```

## Để run được project thì phải làm gì?
**Maven**: 

- Maven là gì? Tự đọc ở [ĐÂY](https://topdev.vn/blog/maven-apache/)
- Cài Maven như thế nào??? Xem ở link trên luôn

**Git**:

- Cần git để quản lý source code, vào [ĐÂY](https://o7planning.org/vi/11707/huong-dan-cai-dat-va-cau-hinh-git-tren-windows) đọc tiếp

**JDK**: Tất nhiên rồi, không có thì làm sao mà chạy được. Cài đặt JDK 8 thôi, cách cài tự search trên mạng


## Trong Project có những gì?

**Lombok**: Thay vì tạo get/set theo cách truyền thống thì mình dùng thư viện lombok để gen 1 cách tự động.
Nó hoạt động như thế nào thì tìm hiểu ở **[ĐÂY](https://viblo.asia/p/gioi-thieu-ve-project-lombok-3P0lPAzp5ox)**
*Nhớ cài đặt Plugin vào IntelliJ hoặc Eclips không code lại đỏ lòm không biết tại sao.*


**Jackson**: Thằng này trong project được dùng để create/load json, tìm hiểu thêm ở **[ĐÂY](https://www.baeldung.com/jackson-object-mapper-tutorial)**

**Serializable**: Serialization đơn giản chỉ là chuyển từ một object tồn tại thành một mảng byte. Mảng byte này đại diện cho class của object, phiên bản của object, và trạng thái của object. Chi tiết hơn nữa thì vào [ĐÂY](https://viblo.asia/p/java-serialization-XL6lAYrDlek)

Một số Anotation sử dụng trong project:

- @RestController: Tìm hiểu ở **[ĐÂY](https://www.baeldung.com/spring-controller-vs-restcontroller)**
- @Component vs @Repository and @Service: Tìm hiểu ở **[ĐÂY](https://www.baeldung.com/spring-component-repository-service)**
- @RequestMapping: Chi tiết ở **[ĐÂY](https://www.baeldung.com/spring-new-requestmapping-shortcuts)**

**Project Structure**


## Tài liệu tham khảo
- [RESTful API là gì ?](https://viblo.asia/p/restful-api-la-gi-1Je5EDJ4lnL)
- [JSON Là Gì và Sử Dụng JSON Như Thế Nào](https://www.codehub.com.vn/JSON-La-Gi-va-Su-Dung-JSON-Nhu-The-Nao)
- [Tìm hiểu về chuỗi dữ liệu JSON](https://viblo.asia/p/tim-hieu-ve-chuoi-du-lieu-json-MJykjQMJePB)
- [Bước đầu tìm hiểu Spring Boot](https://kipalog.com/posts/Buoc-dau-tim-hieu-Spring-Boot)
- [Hướng dẫn lập trình Spring Boot cho người mới bắt đầu](https://o7planning.org/vi/11267/huong-dan-lap-trinh-spring-boot-cho-nguoi-moi-bat-dau)
- [Standard Project Structure for Spring Boot Projects](https://www.javaguides.net/2019/01/standard-project-structure-for-spring-boot-projects.html)
- [Sử dụng git như thế nào?](https://lptech.asia/kien-thuc/git-la-gi-su-dung-git-nang-cao-chuan-git-flow)

## Người hướng dẫn
**`Ths. Nguyễn Ngọc Tiến`** 

